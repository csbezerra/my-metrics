---------------------------------------------------------------------------------------------------
------------------------ Instruções para uso do KittiSeg Iris Segmentation ------------------------
---------------------------------------------------------------------------------------------------

[1] - Treinamento:
    O comando para executar o treinamento está entre --> <--:
        --> nohup python train.py --hypes hypes/file.json --gpus 1 --project nameProject --name nameExperiment <--
    
    Os arquivos de treinamento e validação estão dentro de KittiSeg/DATA/
        [1.1] - file_train.txt: Contém o path de cada imagem seguido pelo path do ground-truth correspondente.
            ex: path/of/the/image/image1.png path/of/the/mask/image1.png
                path/of/the/image/image2.png path/of/the/mask/image2.png
                ...
                path/of/the/image/imageN.png path/of/the/mask/imageN.png

        [1.2] - file_val.txt: Contém o path de cada imagem, da mesma forma que o file_train.txt, entretanto,
            com imagens diferentes. (without overlap)
        
        [1.3] - file_test.txt: Contém apenas o path de cada imagem a ser testada ao final de todo o treinamento (step [1]).
            ex: path/of/the/image/image1.png
                path/of/the/image/image2.png
                ...
                path/of/the/image/imageN.png
    
        [1.4] - Ao final do treinamento, os pesos serão salvos em: KittiSeg/RUNS/nameProject/nameExperiment_nameProject_ano_mes_dia_hora:minuto/

[2] - Teste:
    Temos que alterar no código o diretório que queremos testar (não encontrei uma forma de passá-lo por parâmetros).
    
    [2.1] - Alterar o arquivo KittiSeg/evaluate.py nas linhas:
        line 31: flags.DEFINE_string('RUN', 'nameProject/nameExperiment_nameProject_ano_mes_dia_hora:minuto','Modifier for model parameters.')
        line 33: flags.DEFINE_string('hypes', 'hypes/file.json','File storing model parameters.')
    
    [2.2] - Em seguida, alterar o arquivo KittiSeg/incl/evaluation/kitti_test.py na linha:
        line (a partir da 43): test_file = 'file_test.txt'
        
    O comando para executar o teste está entre --> <-- :
        --> python evaluate.py --gpus 1 --hypes hypes/file.json <--

    O kittiSeg fará o carregamento dos pesos salvos durante o treinamento (step [1.4]).
    Por padrão, são os pesos da ultima iteração (32000). Em seguida, fará um avaliação do modelo,
    com base no conjunto de validação e imprimirá os valores das métricas do próprio KittiSeg. 
    O valor que nos interessa é o BestThresh da linha:
    [train] BestThresh  :  41.1765 (valor de exemplo)

    O BestThresh será usado quando calcularmos as métricas (funções criadas por mim em KittiSeg/my-metrics/src/main.py
    e KittiSeg/my-metrics/src/computeMetrics.py).
    
    [2.3] - Após a impressão das métricas do KittiSeg, as imagens de teste do arquivo file_test.txt serão carregadas e testadas.
        Essas imagens são salvas em: 
        i - KittiSeg/RUNS/nameProject/nameExperiment_nameProject_ano_mes_dia_hora:minuto/test_images/ (predict ground-truth)
        ii - KittiSeg/RUNS/nameProject/nameExperiment_nameProject_ano_mes_dia_hora:minuto/test_images_green/ (resultado da segmentação em green)
        iii - KittiSeg/RUNS/nameProject/nameExperiment_nameProject_ano_mes_dia_hora:minuto/test_images_rb/ (mapa de confiança da segmentação)

[3] - Calculando as métricas:
    [3.1] - Comparamos os ground-truth originais com os predict ground-truth (step [2.3] i).
    
    Comando para calcular as métricas estão entre --> <--:
        -->python main.py -pathGT path/of/the/masks/ -pathMRes path/of/the/nameProject/nameExperiment_nameProject_ano_mes_dia_hora:minuto/test_images/ -img path/of/the/images/ -nm nameToSaveErrorImages -t BestThresh<--
        
    [3.2] - Para cada imagem temos os resultados de todas as métricas conforme:
            REC: 0.976615158879
            TNR: 0.99408296076
            PRE: 0.97344606947
            NPV: 0.994802282919
            FPR: 0.00591703923985
            FNR: 0.0233848411206
            FDR: 0.0265539305302
            ACC: 0.990908333333
            IoU: 0.951272889683
            FSC: 0.975028039095
            ERR: 0.00909166666667
            CM: [[97610   581]
             [  510 21299]]
            Image: nameImage
    
    [3.3] - Ao processar cada imagem, os valores médios e std respectivamente para cada métrica são impressos conforme:
        All images
        REC: 0.971947932577 0.0195513047825
        TNR: 0.99375535657 0.0033039442309
        PRE: 0.968639836961 0.0159039638661
        NPV: 0.99460475218 0.00382823290255
        FPR: 0.0062446434305 0.0033039442309
        FNR: 0.0280520674227 0.0195513047825
        FDR: 0.0313601630393 0.0159039638661
        ACC: 0.990300916667 0.00405785091161
        IOU: 0.942269104256 0.0236480786885
        FSC: 0.970121220894 0.0127616936858
        ERR: 0.00969908333333 0.00405785091161
        Qtd of the pixels: 24000000
        BestTresh 0-255: 135
        [INFO] Terminated in 0.6512236317 minuts
     
     Onde:
        REC = recall; TNR = true negative rate; PRE: precision; NPV: ; FPR: palse positive rate; FNR: false negative rate;
        FDR: ; ACC: accuracy; IOU: intersection over union; FSC: f1score; ERR: error (NICE1 protocol);        
        
    [3.4] - Esses valores médios é que são reportados no paper. São os valores finais da base de teste.
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
