#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Plots MaxF1 score.
-------------------------------------------------
The MIT License (MIT)
Copyright (c) 2017 Marvin Teichmann
More details: https://github.com/MarvinTeichmann/KittiSeg/blob/master/LICENSE
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import re
import numpy as np
import sys

import matplotlib.pyplot as plt

#/home/csbezerra/KittiSeg/RUNS/nice1/k5_bs1_niceI_k0_2018_01_19_11.09/output.log
runfolder = '/home/csbezerra/KittiSeg/RUNS/'
currun = 'nice1/k5_bs1_niceI_k0_2018_01_19_11.09'
anafile = 'output.log'

output_folder = '/home/csbezerra/KittiSeg/my_metrics'
name = 'segmentation'

outname = os.path.join(output_folder, name)

filename = os.path.join(runfolder, currun, anafile)
eval_iters = 400
max_iters = 36000


def read_values(tad, prop, typ):
    #regex_string = "%s\s+\(%s\)\s+:\s+(\d+\.\d+)" % (prop, typ)
    regex_string = "\[%s\]\s+\%s\s+\(%s\)\s+:\s+(\d+\.\d+)" % (tad, prop, typ)
    regex = re.compile(regex_string)

    value_list = [regex.search(line).group(1) for line in open(filename) if regex.search(line) is not None]

    float_list = [float(value) for value in value_list]
    
    print (np.shape(float_list))

    return float_list


label_list = xrange(eval_iters, max_iters+1, 100)

plt.figure(figsize=(8, 5))
plt.title(u'Avaliação dos classificadores (Métrica Fmax)')
plt.rcParams.update({'font.size': 12})
print ("teste")

plt.plot(label_list, read_values('MaxF1', 'raw'), label='MaxF1 (Raw)', marker=".", color='blue', linestyle=' ')
plt.plot(label_list, read_values('MaxF1', 'smooth'), label='MaxF1 (Smoothed)', color='blue')
plt.plot(label_list, read_values('Average Precision', 'raw'), label='Average Precision (Raw)', color='red', marker=".", linestyle=' ')
plt.plot(label_list, read_values('Average Precision', 'smooth'), label='Average Precision (Smoothed)', color='red')
plt.xlabel('Iteration')
plt.ylabel('Validation Score [%]')
plt.legend(loc=0)


plt.savefig(outname + ".pdf")
plt.savefig(outname + ".pgf")

plt.show()
