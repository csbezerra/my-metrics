from PIL import Image
import skimage.io as io
from sklearn import metrics
#from scipy.spatial import distance
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
import numpy as np
import pylab as pl

# ''' -------------------------- Computing confusion matrix and metrics using scikit library -------------------------- '''
# def computeConfusionMatrix_scikit(genuineLabels, predictLabels):
    # # Confusion Matrix
    # print ("Computing Confusion Matrix...")
    # confusionMatrix = metrics.confusion_matrix(genuineLabels, predictLabels)
    
    # # print (confusionMatrix)

    # listaValues = allMetrics(confusionMatrix)
    # return confusionMatrix, listaValues

def allMetrics(genuineLabels, predictLabels):
    # Computing Confusion Matrix
    confusionMatrix = metrics.confusion_matrix(genuineLabels, predictLabels, [0, 1])

    TN = float(confusionMatrix[0][0])
    FN = float(confusionMatrix[1][0])
    TP = float(confusionMatrix[1][1])
    FP = float(confusionMatrix[0][1])

    # Sensitivity, hit rate, recall, or true positive rate
    try:
        REC = TP/(TP+FN)
    except ZeroDivisionError:
        REC = 0.0
    # Specificity or true negative rate
    try:
        TNR = TN/(TN+FP)
    except ZeroDivisionError:
        TNR = 0.0
    # Precision or positive predictive value
    try:
        PRE = TP/(TP+FP)
    except ZeroDivisionError:
        PRE = 0.0
    # Negative predictive value
    try:
        NPV = TN/(TN+FN)
    except ZeroDivisionError:
        NPV = 0.0
    # Fall out or false positive rate
    try:
        FPR = FP/(FP+TN)
    except ZeroDivisionError:
        FPR = 0.0
    # False negative rate
    try:
        FNR = FN/(TP+FN)
    except ZeroDivisionError:
        FNR = 0.0
    # False discovery rate
    try:
        FDR = FP/(TP+FP)
    except ZeroDivisionError:
        FDR = 0.0
    # Overall accuracy
    try:
        ACC = (TP+TN)/(TP+FP+FN+TN)
    except ZeroDivisionError:
        ACC = 0.0
    #Intersection over Union (paper The Pascal VOC Challenge - M. Everingham)
    try:
        IoU = TP / (TP+FP+FN)
    except ZeroDivisionError:
        IoU = 0.0
    # Compute the F1 score in valid values (i.e. != 0.0) to PRE and REC
    try:
        FSC = 2 * ((PRE * REC) / (PRE + REC))
    except ZeroDivisionError:
        FSC = 0.0

    # Error of segmentation NICE.I protocoll
    ERR = computeAverageError(genuineLabels, predictLabels)

    # print ('\n- REC: {0}\n- TNR: {1}\n- PRE: {2}\n- NPV: {3}\n- FPR: {4}\n- FNR: {5}\n- FDR: {6}\n- ACC: {7}\n- IoU: {8}\n- F-score: {9}'.format(REC, TNR, PRE, NPV, FPR, FNR, FDR, ACC, IoU, FSC))
    listValues = map(float,(REC, TNR, PRE, NPV, FPR, FNR, FDR, ACC, IoU, FSC, ERR))

    return confusionMatrix, listValues

def computeAllResults(listAllValues):
    # Lists for each metric
    recList = []
    tnrList = []
    preList = []
    npvList = []
    fprList = []
    fnrList = []
    fdrList = []
    accList = []
    iouList = []
    fscoreList = []
    errList = []
    cmList = []
    nameList = []

    for byImage in listAllValues:
        # Get each value from metric in the list id 
        recList.append(byImage[0])
        tnrList.append(byImage[1])
        preList.append(byImage[2])
        npvList.append(byImage[3])
        fprList.append(byImage[4])
        fnrList.append(byImage[5])
        fdrList.append(byImage[6])
        accList.append(byImage[7])
        iouList.append(byImage[8])
        fscoreList.append(byImage[9])
        errList.append(byImage[10])
        cmList.append(byImage[11])
        nameList.append(byImage[12])
    # Print Average and std results respectivelly 
    print 'All images'
    print 'REC:', np.mean(recList) , np.std(recList)
    print 'TNR:', np.mean(tnrList), np.std(tnrList)
    print 'PRE:', np.mean(preList), np.std(preList)
    print 'NPV:', np.mean(npvList), np.std(npvList)
    print 'FPR:', np.mean(fprList), np.std(fprList)
    print 'FNR:', np.mean(fnrList), np.std(fnrList)
    print 'FDR:', np.mean(fdrList), np.std(fdrList)
    print 'ACC:', np.mean(accList), np.std(accList)
    print 'IOU:', np.mean(iouList), np.std(iouList)
    print 'FSC:', np.mean(fscoreList), np.std(fscoreList)
    print 'ERR:', np.mean(errList), np.std(errList)
    print 'Qtd of the pixels:', np.sum(cmList)
    # print nameList

    # plotPrecisionRecallCurve(preList, recList, aucScorePR = 0.9999 , nameExp='handl_ubiris_120')

    return

def computeClassificationReport(genuineLabels, predictLabels):
    print metrics.classification_report(genuineLabels, predictLabels, digits=12)
    # print metrics.precision_recall_fscore_support(genuineLabels, predictLabels, average='binary')
    return

def computeAverageError(genuineLabels, predictLabels):
    # Error of the segmentation each image (hamming distance)
    # print ("Computing Average Error...")
    errorImage = metrics.zero_one_loss(genuineLabels, predictLabels)
    return errorImage

def computeAccuracy(genuineLabels, predictLabels):
    # Accuracy
    # print ("Computing Accuracy...")
    accuracy = metrics.accuracy_score(genuineLabels, predictLabels)
    return accuracy

def computePrecisionScore(genuineLabels, predictLabels):
    # Precision score
    # print ("Computing Precision score...")
    precisionScore = metrics.precision_score(genuineLabels, predictLabels)
    return precisionScore

def computeRecallScore(genuineLabels, predictLabels):
    # Recall score
    # print ("Computing Recall score...")
    recallScore = metrics.recall_score(genuineLabels, predictLabels)
    return recallScore

def computeFscore(genuineLabels, predictLabels):
    # F-score
    # print ("Computing F-score...")
    f_score = metrics.f1_score(genuineLabels, predictLabels)#, average='binary'
    return f_score

def computePrecisionRecallCurve(genuineLabels, predictLabels, nameExp):
    print ("Computing Precision-Recall Curve")
    # Precision-Recall Curve
    precision, recall, thresholds = metrics.precision_recall_curve(genuineLabels, predictLabels)

    aucScorePR = metrics.average_precision_score(genuineLabels, predictLabels)

    #f_score = []
    #for i in xrange(len(thresholds)):
        #f_score.append(compute_fscore(precision[i], recall[i]))

    # Function to plot recall-precision curve
    plotPrecisionRecallCurve(precision, recall, aucScorePR, nameExp)
    return precision, recall, thresholds

def compute_fscore(prec, rec):
    f_score = 2 * (prec * rec) / (prec + rec)
    return f_score

def plotPrecisionRecallCurve(precision, recall, aucScorePR, nameExp):
    # plot the main figure
    fig, ax = plt.subplots(figsize=(5,5))
    ax.plot(recall, precision, 'b--', label='area sobre a curva = %0.4f' % aucScorePR)
    ax.plot([0, 1], [0, 1], color='gray', lw=0.5, linestyle=':')
    ax.legend(loc="lower right")

    area_zoom = zoomed_inset_axes(ax, 2.5, loc=2) # zoom-factor: 2.5, location: upper-left
    area_zoom.plot(recall, precision, 'b--')
    area_zoom.plot([0, 1], [0, 1], color='gray', lw=0.5, linestyle=':')

    area_zoom.set_xlim(0.8, 1.0) # apply the x-limits
    area_zoom.set_ylim(0.8, 1.0) # apply the y-limits
    mark_inset(ax, area_zoom, loc1=2, loc2=4, fc="none", ec="r", lw=0.1)
    
    plt.xticks(visible=False)
    plt.yticks(visible=False)
    ax.set_xlabel('Recall')
    ax.set_ylabel('Precision')
    ax.set_title('Precision-recall Curve')
    plt.grid(linestyle=':', lw=0.2)
    fig.savefig('../results/' + nameExp + '/' + nameExp +'_precision-recall_curve.eps')

def computeRocCurve(genuineLabels, predictLabels):
    print ("Receiver Operator Characteristic - ROC")
    fpr, tpr, thresholds = metrics.roc_curve(genuineLabels, predictLabels, pos_label=1)
    aucScore = metrics.roc_auc_score(genuineLabels, predictLabels)
    plotRocCurve(fpr, tpr, aucScore)
    return

def plotRocCurve(fpr, tpr, aucScore):
    # plot the main figure
    fig, ax = plt.subplots(figsize=(5,5))
    ax.plot(fpr, tpr, 'b--', label='area sobre a curva = %0.4f' % aucScore)
    ax.plot([0, 1], [0, 1], color='gray', lw=0.5, linestyle=':')
    ax.legend(loc="lower right")

    area_zoom = zoomed_inset_axes(ax, 2.5, loc=1) # zoom-factor: 2.5, location: upper-left
    area_zoom.plot(fpr, tpr, 'b--')
    area_zoom.plot([0, 1], [0, 1], color='gray', lw=0.5, linestyle=':')

    area_zoom.set_xlim(0.0, 0.1) # apply the x-limits
    area_zoom.set_ylim(0.9, 1.0) # apply the y-limits
    mark_inset(ax, area_zoom, loc1=2, loc2=4, fc="none", ec="r", lw=0.1)

    plt.xticks(visible=False)
    plt.yticks(visible=False)
    ax.set_xlabel('False Positive Rate')
    ax.set_ylabel('True Positive Rate')
    ax.set_title('Receiver Operator Characteristic - ROC')
    plt.grid(linestyle=':', lw=0.2)
    fig.savefig('../results/' + nameExp + '/' + nameExp + '_roc_curve.eps')
    return











