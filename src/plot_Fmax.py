#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Plots MaxF1 score.

-------------------------------------------------

The MIT License (MIT)

Copyright (c) 2017 Marvin Teichmann

More details: https://github.com/MarvinTeichmann/KittiSeg/blob/master/LICENSE
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import re
import numpy as np
import sys
import matplotlib
matplotlib.rcParams['text.latex.unicode'] = True
import matplotlib.pyplot as plt
import argparse

reload(sys)  
sys.setdefaultencoding('utf8')

def get_args():
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--project", type=str, required=True,
        help="path of the project. Normally KittiSeg/RUNS/...")
    ap.add_argument("-n", "--name", type=str, required=True,
        help="path of the experiment name. Normally KiitiSeg/RUNS/project/")
    ap.add_argument("-k0", "--kfold_0", type=str, required=True,
        help="Experiment name of the [0] k-fold")
    ap.add_argument("-k1", "--kfold_1", type=str, required=False,
        help="Experiment name of the [1] k-fold")
    ap.add_argument("-k2", "--kfold_2", type=str, required=False,
        help="Experiment name of the [2] k-fold")
    ap.add_argument("-k3", "--kfold_3", type=str, required=False,
        help="Experiment name of the [3] k-fold")
    ap.add_argument("-k4", "--kfold_4", type=str, required=False,
        help="Experiment name of the [4] k-fold")    
    ap.add_argument("-evi", "--evalIters", type=int, default=400,
        help="Number of iterations for each evaluation. Obs: eval_iters parameter")
    ap.add_argument("-mi", "--maxIters", type=int, default=32000,
        help="Number of total iterations for training. Obs: max_iters parameter")
    args = vars(ap.parse_args())

    return args["project"], args["name"], args["kfold_0"], args["kfold_1"], args["kfold_2"], args["kfold_3"], args["kfold_4"], args["evalIters"], args["maxIters"]

#eval_iters = 400
#max_iters = 32000

def read_values(filename, tad, prop, typ):
    regex_string = "\[%s\]\s+\%s\s+\(%s\)\s+:\s+(\d+\.\d+)" % (tad, prop, typ)
    regex = re.compile(regex_string)

    value_list = [regex.search(line).group(1) for line in open(filename) if regex.search(line) is not None]

    float_list = [float(value) for value in value_list]

    return float_list

def plotGraphLR(project, name, kfold_0,  kfold_1, kfold_2, kfold_3, kfold_4, eval_iters, max_iters):

    label_list = xrange(eval_iters, max_iters+1, eval_iters)
    kfoldsList = [kfold_0, kfold_1, kfold_2, kfold_3, kfold_4]
    #kfoldsList = ['k5_bs1_niceI_k0_2018_01_19_11.09', 'k5_bs1_niceI_k1_2018_01_19_14.36', 'k5_bs1_niceI_k2_2018_01_19_18.13', 'k5_bs1_niceI_k3_2018_01_19_21.43', 'k5_bs1_niceI_k4_2018_01_20_01.11']

    plt.figure(figsize=(6, 3.5))
    #plt.title(u'Avaliação dos classificadores (Métrica Fmax)')
    plt.rcParams.update({'font.size': 7})

    listaLog = []
    for kfold in kfoldsList:
        if kfold != None:
            listaLog.append("/home/csbezerra/KittiSeg/RUNS/"+project+"/"+kfold+"/"+"output.log")
    
    #listaLog.append("/home/csbezerra/KittiSeg/RUNS/nice1/k5_bs1_niceI_k0_2018_01_19_11.09/output.log")
    #listaLog.append("/home/csbezerra/KittiSeg/RUNS/nice1/k5_bs1_niceI_k1_2018_01_19_14.36/output.log")
    #listaLog.append("/home/csbezerra/KittiSeg/RUNS/nice1/k5_bs1_niceI_k2_2018_01_19_18.13/output.log")
    #listaLog.append("/home/csbezerra/KittiSeg/RUNS/nice1/k5_bs1_niceI_k3_2018_01_19_21.43/output.log")
    #listaLog.append("/home/csbezerra/KittiSeg/RUNS/nice1/k5_bs1_niceI_k4_2018_01_20_01.11/output.log")
    #listaLog.append("/output.log")

    listaCor = ['blue', 'red', 'green', 'darkmagenta', 'cyan']
    linestyles = ['--', '--', '--', '--', '--']
    markers = ['v', '*', '8', 's', 'p']
    #markers = ['+', '+', '+', '+', '+']

    label = []
    meanFolds = []
    for indice in xrange(len(listaLog)):
        #label.append('Train. clas. '+str(indice+1))
        label.append('Fscore k = '+ str(indice))
        listDataValRaw = read_values(listaLog[indice], 'val', 'Fscore', 'raw')
        meanFolds.append(max(listDataValRaw))
        #plt.plot(label_list, listDataValRaw, marker=markers[indice], markersize=2.5, markeredgewidth=0.0, color=listaCor[indice], linestyle=' ')
        #plt.plot(label_list, listDataValRaw, marker=markers[indice], markersize=2.5, markeredgewidth=0.0, color=listaCor[indice], linestyle='-', lw=0.7)
        #label.append('Fscore (smooth)')
        listDataValSmooth = read_values(listaLog[indice], 'val', 'Fscore', 'smooth')
        #plt.plot(label_list, listDataValSmooth, marker=markers[indice], markersize=2.5, markeredgewidth=0.0, color=listaCor[indice], linestyle='-', lw=0.7)
        print("K-fold: {} | Best Weight: {} | Iteration index: {} (Raw)".format(indice+1, max(listDataValRaw), (eval_iters*(np.argmax(listDataValRaw)+1))))
        #print("K-fold: {} | Best Weight: {} | Iteration index: {} (Smoothed)".format(indice+1, max(listDataValSmooth), (eval_iters*(np.argmax(listDataValSmooth)+1))))
    print ("Mean of the folds: {}".format(np.mean(meanFolds)))
    print ("Std of the folds: {}".format(np.std(meanFolds)))

    #plt.yticks(np.array(range(0, 81, 10)))
    #plt.legend(label, ncol=2, loc="lower right", columnspacing=0.5, labelspacing=0.0, handletextpad=0.0, handlelength=1.0, fancybox=True, shadow=True, prop={'size': 12})
    #plt.legend(label, ncol=5, bbox_to_anchor=(0.15, 0.15, 0.85, 0.0), columnspacing=0.5, labelspacing=0.0, handletextpad=0.0, handlelength=1.0, fancybox=True, shadow=True, prop={'size': 12})
    plt.legend(label, loc="lower right", shadow = True, fancybox=True)
    plt.xlabel('Iteration') 
    plt.ylabel('Validation Score [%]')
    plt.grid(linestyle=':', lw=0.2)
    plt.xlim((0, max_iters))
    
    # Colocar os nomes e project pra salvar junto
    plt.savefig("../"+ project +"_"+ name +".eps", bbox_inches='tight')

    return

def main():
    # get parameters
    project, name, kfold_0,  kfold_1, kfold_2, kfold_3, kfold_4, eval_iters, max_iters = get_args()
    
    plotGraphLR(project, name, kfold_0,  kfold_1, kfold_2, kfold_3, kfold_4, eval_iters, max_iters)








if __name__ == '__main__':
    main()
