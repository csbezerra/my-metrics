import os
import sys
from PIL import Image, ImageDraw
import numpy as np
import skimage.io as io
import argparse
import time
import concurrent.futures
import multiprocessing

import computeMetrics

def get_args():
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-pathGT", "--pathGroundTruth", required=True,
        help="path to original groundTruth dataset")
    ap.add_argument("-pathMRes", "--pathMaskPredict", required=True,
        help="path to mask predict output")
    ap.add_argument("-img", "--pathImage", required=True,
        help="path to eye images")
    ap.add_argument("-nm", "--name", required=True,
        help="Name to experiment")
    ap.add_argument("-t", "--bestTresh", type=float, required=True,
        help="Value of the best threshold")
    ap.add_argument("-p", "--plot", type=str, default="plot.eps",
        help="path to output ROC and Recall-Precision plot")
    args = vars(ap.parse_args())

    return args["pathGroundTruth"], args["pathMaskPredict"], args["pathImage"], args["name"], args["bestTresh"]


''' ---------------------------------------------------- load_masks ---------------------------------------- '''
def load_mask_genuine(pathGT):
    image_paths = [os.path.join(pathGT, f) for f in os.listdir(pathGT) if os.path.isfile(os.path.join(pathGT,f))]
    ## images will contains face images
    #images_mask = []
    subjects_mask = []
    for image_path in image_paths:
        #imgMask = np.array(Image.open(image_path).convert('L'))
        ## Get the label of the image
        nbr_mask = os.path.split(image_path)[1].split("_segmented.")[0].replace("","")
        ##nbr_mask = os.path.split(image_path)[1].split(".")[0].replace("","")

        #images_mask.append(imgMask)
        subjects_mask.append(nbr_mask)
    # return the images list
    return subjects_mask

def changePixelsBestTreshold(imgMaskPredict, bestTresh):
    # Get rows and cols of the image
    row, col, _ = np.shape(imgMaskPredict)

    for i in xrange (int(row)):
        for j in xrange (int(col)):
            if imgMaskPredict[i][j] >= bestTresh:
                imgMaskPredict[i][j] = 1
            else:
                imgMaskPredict[i][j] = 0
    return imgMaskPredict

''' ---------------------------------------------------- load_images ---------------------------------------- '''
def loadImages(pathGT, pathMaskPre, pathImage, subject_mask, nameExp, bestTresh):
    
    imgEye = Image.open(pathImage + subject_mask + '.jpg')
    # imgMaskGenuine = np.array(Image.open(pathGT + subject_mask + '_mask.png').convert('L'))
    imgMaskGenuine = np.array(Image.open(pathGT + subject_mask + '.tiff').convert('L'))
    imgMaskPredict = np.array(Image.open(pathMaskPre + subject_mask + '_segmented.png').convert('L'))
    #imgMaskPredict = np.array(Image.open(pathMaskPre + subject_mask + '.defects.png').convert('L'))

    #imgMaskPredict = changePixelsBestTreshold(imgMaskPredict, bestTresh)

    # Convert the pixels ([0, 1] for negative and positive class) to form a binary image
    imgMaskGenuine[np.where(imgMaskGenuine <= 128)] = 0
    imgMaskGenuine[np.where(imgMaskGenuine > 128)] = 1
    
    imgMaskPredict[np.where(imgMaskPredict <= bestTresh)] = 0
    imgMaskPredict[np.where(imgMaskPredict > bestTresh)] = 1

    # Draw the errors (FP and FN) in the original image
    drawErrorsIntoImage(imgEye, imgMaskGenuine, imgMaskPredict, subject_mask, nameExp)

    # listGenuineLabels.extend(np.ravel(imgMaskGenuine))
    # listPredictLabels.extend(np.ravel(imgMaskPredict))

        # del imgMaskGenuine, imgMaskPredict
    # del subjects_mask

    # precision, recall, thresholds = computeMetrics.computePrecisionRecallCurve(listGenuineLabels, listPredictLabels)

    #for i in thresholds:
    #print ('{0} vs {1}'.format(np.mean(precision), np.mean(recall)))

    # computeMetrics.computeRocCurve(listGenuineLabels, listPredictLabels)

    #for i in xrange(len(listPredictLabels)):
        #if listPredictLabels[i] <= 128:
            #listPredictLabels[i] = 0
        #else:
            #listPredictLabels[i] = 1
    
    # confusionMatrix, listaValues = computeMetrics.computeConfusionMatrix_scikit(np.ravel(imgMaskGenuine), np.ravel(imgMaskPredict))

    # Compute all metrics and confusion matrix for each image  
    confusionMatrix, listValues = computeMetrics.allMetrics(np.ravel(imgMaskGenuine), np.ravel(imgMaskPredict))
    listValues.append(confusionMatrix)
    listValues.append(subject_mask)

    # Print on the screen the results from metrics
    print_metricsByImage(listValues)
    
    return listValues


def print_metricsByImage(listValues):
    list_names = ('REC:', 'TNR:', 'PRE:', 'NPV:', 'FPR:', 'FNR:', 'FDR:', 'ACC:', 'IoU:', 'FSC:', 'ERR:', 'CM:', 'Image:')
    for metric, value in zip(list_names,listValues):
        print metric, value
    print

    return

def drawErrorsIntoImage(imgEye, imgMaskGenuine, imgMaskPredict, subject_mask, nameExp):
    imgEyeCopy = imgEye.copy()
    # Convert the image to array to access the pixels
    imgEyeCopy = np.array(imgEyeCopy)

    # Get rows and cols of the image
    row, col, _ = np.shape(imgEyeCopy)

    for i in xrange (int(row)):
        for j in xrange (int(col)):
            # False Positive pixel
            if imgMaskGenuine[i][j] == 0 and imgMaskPredict[i][j] == 1:
                imgEyeCopy[i][j] = (0, 255, 0)
                # print imgEyeCopy[i][j]
            # False Negative pyxel
            elif imgMaskGenuine[i][j] == 1 and imgMaskPredict[i][j] == 0:
                imgEyeCopy[i][j] = (255, 0, 0)
                # print imgEyeCopy[i][j]
            # True Positive pixel
            # elif imgMaskGenuine[i][j] == 1 and imgMaskPredict[i][j] == 1:
                # imgEyeCopy[i][j] = (0, 0, 0)

    img = Image.fromarray(imgEyeCopy, mode='RGB')
    # img_draw = ImageDraw.Draw(img)
    # img_draw.text((70, 250), 'E:', fill='green')
    img.save('../results/' + nameExp + '/' + subject_mask + '.png')

    return


def main():
    # get parameters
    pathGT, pathMaskPre, pathImage, nameExp, bestTresh = get_args()
    
    bestTresh = int((bestTresh / 100) * 255)
    print (bestTresh)

    if nameExp and not os.path.exists('../results/' + nameExp):
        os.makedirs('../results/' + nameExp)

    startTime = time.time()
    print (time.asctime( time.localtime(time.time())))
    print ("[INFO] GroundTruth in path: {0}".format(pathGT))
    print ("[INFO] Predict mask in path: {0}".format(pathMaskPre))

    # Loading the masks to load the images in the same order
    subjects_mask = load_mask_genuine(pathMaskPre)

    listAllValues = []

    # Parallel mode
    pool = concurrent.futures.ProcessPoolExecutor(multiprocessing.cpu_count())
    futures = []
    # loop over the input images
    for subject_mask in subjects_mask:
        # load the image, pre-process it, and store it in the data list
        futures.append(pool.submit(loadImages, pathGT, pathMaskPre, pathImage, subject_mask, nameExp, bestTresh))
    for x in concurrent.futures.as_completed(futures):
        listAllValues.append(x.result())
        
    #for subject_mask in subjects_mask:
        #listValues = loadImages(pathGT, pathMaskPre, pathImage, subject_mask, nameExp, bestTresh)
        #listAllValues.append(listValues)

    #print (listAllValues)

    computeMetrics.computeAllResults(listAllValues)
    print ("BestTresh 0-255: {}".format(bestTresh))

    # computeMetrics.computePrecisionRecallCurve(listGenuineLabels, listPredictLabels, nameExp)

    print ("[INFO] Terminated in {0} minuts".format((time.time() - startTime) / 60))







if __name__ == '__main__':
    main()

''' https://github.com/MarvinTeichmann/KittiSeg/blob/master/README.md#manage-data-storage '''

